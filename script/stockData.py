# -*- coding: iso-8859-15 -*-
import concurrent.futures
from multiprocessing import Process
import csv
import time
from datetime import date
from datetime import datetime
import sqlite3
import codecs
import unicodedata
import sys

dbpath = 'report2013.db'

  # select idProdutoFarmacia,NomeProdutoInterno, sum(QtdEntregue), (sum(QtdEntregue)/12) as media, count(*) as f, (sum(QtdEntregue)/12) as media, ((sum(QtdEntregue)/12)*count(*)) as Em FROM saida2013 group by idProdutoFarmacia;

db = sqlite3.connect(dbpath, timeout=2)
c = db.cursor()
## enable utf8
c.execute('PRAGMA encoding = "UTF-8";');
db.row_factory = sqlite3.Row

cur = db.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS product_stats(idProdutoFarmacia INTEGER AUTO_INCREMENT, c INTEGER, I INTEGER, Em Double, EMax DOUBLE, pp Double, q double  );")

db.commit();
One = {}

rowtitle1 = ['Data', 'idProdutoFarmacia', 'NomeProdutoInterno', 'Baixo','Media', 'MAlto', \
    'QtdeEntrada', 'TipoMedicamento','TipoGenerico','TipoMaterial', \
    'idMedicamentoPrincipioAtivo', 'QtdeEntrega']
rowtitle2 = ['Data', 'idProdutoFarmacia', 'NomeProdutoInterno', 'QtdeEntrega', 'Baixo', 'Media', 'MAlto' \
    ,'QtdeEntrada', 'TipoMedicamento','TipoGenerico','TipoMaterial', \
    'idMedicamentoPrincipioAtivo' \
        ]

# Consumo Médio Mensal (c) - média aritmética do consumo nos últimos 12 meses;
c = 0;
# Tempo de Aquisição (T)- período decorrido entre a emissão do pedido de compra e o recebimento do material no Almoxarifado (relativo, sempre, à unidade mês);
T = 1;
# Intervalo de Aquisição (I)- período compreendido entre duas aquisições normais e sucessivas;
I = 0;
# fração (f) do tempo de aquisição que deve, em princípio, variar de 0,25 de T a 0,50 de T;
f = 0.50;

# Estoque Mínimo ou de Segurança (Em)- é a menor quantidade de material a ser mantida em estoque capaz de atender a um consumo superior ao estimado para um certo período \
# ou para atender a demanda normal em caso de entrega da nova aquisição. É aplicável tão somente aos itens indispensáveis aos serviços do órgão ou entidade. Obtém-se multiplicando o consumo médio mensal por uma 
# fração (f) do tempo de aquisição que deve, em princípio, variar de 0,25 de T a 0,50 de T;
def Em(c, f): return (float(c)*float(f))

# Estoque Máximo (EM) - a maior quantidade de material admissível em estoque, suficiente para o consumo em certo períod
def EM(em,c,I): return (int(em)+int(c)*int(I))

# Ponto de pedido (Pp) - Nível de Estoque que, ao ser atingido, determina imediata emissão de um pedido de compra, visando a recompletar o Estoq    ue Máximo.
def Pp(em, c , T): return int(em)+int(c)*int(T)

# Quantidade a ressuprir  (Q) - número de unidades adquirir para recompor o Estoque Máximo. Obtém-se multiplicando o Consumo Médio Mensal pelo In    tervalo de Aquisição.
def Q(c, I): return (int(c)*int(I))

def dayToMonth(day): return (day/30)

def getConsumoMensal(idprod):
  cur = db.cursor()
  cur.execute('SELECT (SUM(QtdEntregue)/12) as media FROM saida2013 WHERE idProdutoFarmacia = ?', [idprod])
  Res = cur.fetchone()
  return Res['media']

def getIntervaloAquisicao(idprod):
  cur = db.cursor()
  cur.execute('SELECT Data FROM saida2013 WHERE idProdutoFarmacia = ? limit 2', [idprod])
  Datas = cur.fetchall()
  x=0
  D = [0,1]
  for data in Datas:
     D[x] = data['Data']+'-01'
     x=x+1

  if (D[0] == 0 or D[1] == 1):
    return 0

  D[1] = D[1].split('-')
  D[0] = D[0].split('-')
  d1 = datetime(int(D[1][0]), int(D[1][1]), int(D[1][2]))
  d2 = datetime(int(D[0][0]), int(D[0][1]), int(D[0][2]) )
  differ = d1-d2
  d = str(differ)
  d = d.split("days")
  return d[0]

def insertProd(prod):
  cur = db.cursor()
  cur.execute("INSERT INTO product_stats (idProdutoFarmacia, c, I, Em, EMax, pp, q) VALUES (?, ?, ?, ?, ?, ?, ?)", \
      [prod['idProdutoFarmacia'], prod['c'], prod['I'], prod['Em'], prod['EMax'], prod['pp'], prod['q']] \
      )
  db.commit()



cur = db.cursor()
cur.execute("SELECT idProdutoFarmacia, NomeProdutoInterno FROM entrada2013 GROUP BY idProdutoFarmacia");
Products = cur.fetchall()

Lista = []

for product in Products:
  p = {}
  p['idProdutoFarmacia'] = product['idProdutoFarmacia']
  p['NomeProdutoInterno'] = product['NomeProdutoInterno']

  c = getConsumoMensal(product['idProdutoFarmacia'])
  I = getIntervaloAquisicao(product['idProdutoFarmacia'])
  p['c'] = c
  p['I'] = I
  if (c!=0 and c != None):
    p['Em'] = Em(c, f)
    p['EMax'] = EM(p['Em'], c,I)
    p['pp'] = Pp(p['Em'], c, T)
    p['q'] = Q(c,I)
  else:
    p['Em'] = 0
    p['EMax'] = 0
    p['pp'] = 0
    p['q'] = 0

  Lista.append(p)
  print('>> ' + product['NomeProdutoInterno'] + " c = " + str(p['c']) + " I = " + str(p['I']) + " Q = " + str(p['q']) + " EM = " + str(p['EMax']) + " Em = " + str(p['Em']) + " Pp = " + str(p['pp']) )
  insertProd(p)

