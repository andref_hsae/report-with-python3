# -*- coding: iso-8859-15 -*-
import concurrent.futures
from multiprocessing import Process
import csv
import time
from datetime import datetime
import sqlite3
import codecs
import unicodedata
import sys

dbpath = 'report2013.db'

  # select idProdutoFarmacia,NomeProdutoInterno, sum(QtdEntregue), (sum(QtdEntregue)/12) as media, count(*) as f, (sum(QtdEntregue)/12) as media, ((sum(QtdEntregue)/12)*count(*)) as Em FROM saida2013 group by idProdutoFarmacia;

db = sqlite3.connect(dbpath, timeout=2)
c = db.cursor()
## enable utf8
c.execute('PRAGMA encoding = "UTF-8";');
c.execute('CREATE TABLE IF NOT EXISTS report2013 (Data text, idProdutoFarmacia integer, \
    NomeProdutoInterno text, valBaixo double, valMedio double, valMAlto double, QtdeEntrada integer, \
    TipoMedicamento boolean, TipoGenerico Boolean, TipoMaterial Boolean, idMedicamentoPrincipioAtivo integer \
    , QtdEntregue integer);');

c.execute('CREATE TABLE IF NOT EXISTS entrada2013 (Data text, idProdutoFarmacia integer, \
    NomeProdutoInterno text, valBaixo double, valMedio double, valMAlto double, QtdeEntrada integer, \
    TipoMedicamento boolean, TipoGenerico Boolean, TipoMaterial Boolean, idMedicamentoPrincipioAtivo integer \
    , QtdEntregue integer);');

c.execute('CREATE TABLE IF NOT EXISTS saida2013 (Data text, idProdutoFarmacia integer, \
    NomeProdutoInterno text, valBaixo double, valMedio double, valMAlto double \
    , QtdEntregue integer);');

db.commit()

One = {}

rowtitle1 = ['Data', 'idProdutoFarmacia', 'NomeProdutoInterno', 'Baixo','Media', 'MAlto', \
    'QtdeEntrada', 'TipoMedicamento','TipoGenerico','TipoMaterial', \
    'idMedicamentoPrincipioAtivo', 'QtdeEntrega']
rowtitle2 = ['Data', 'idProdutoFarmacia', 'NomeProdutoInterno', 'QtdeEntrega', 'Baixo', 'Media', 'MAlto' \
    ,'QtdeEntrada', 'TipoMedicamento','TipoGenerico','TipoMaterial', \
    'idMedicamentoPrincipioAtivo' \
        ]

def readOne(One):
  print("reading one..")
  csvfile = open("../MateriaisEMedicamentos_Entrada.csv", 'r')
  itemsFileOld = csv.DictReader(csvfile, rowtitle1, dialect='excel', delimiter=';')
  x = 0
  for row in itemsFileOld:
    if (x==0):
      x=x+1
      continue
    row['QtdeEntrega'] = 0
    One[row['Data']]= {}
    One[row['Data']][int(row['idProdutoFarmacia'])] = row

    db.execute("INSERT INTO entrada2013 (Data, idProdutoFarmacia, NomeProdutoInterno, \
          valBaixo, valMedio, valMAlto, QtdeEntrada, TipoMedicamento, TipoGenerico, \
          TipoMaterial, idMedicamentoPrincipioAtivo, QtdEntregue ) \
          VALUES \
              (?,?,?,?,?,?,?,?,?,?,?,?)", \
              [row['Data'], row['idProdutoFarmacia'], row['NomeProdutoInterno'], \
              row['Baixo'], row['Media'], row['MAlto'], row['QtdeEntrada'], \
              row['TipoMedicamento'], row['TipoGenerico'], \
              row['TipoMaterial'], row['idMedicamentoPrincipioAtivo'], row['QtdeEntrega'] \
              ])
    db.commit()

  return One

def readTwo(One):
  print('reading two')
  csvfile = open("../MateriaisEMedicamentos_Saida.csv", 'r')

  itemsFile = csv.DictReader(csvfile, rowtitle2, dialect='excel', delimiter=';')
  x=0
  debs = 0
  for row in itemsFile:
    if (x==0):
      x=x+1
      continue

    db.execute("INSERT INTO saida2013 (QtdEntregue, idProdutoFarmacia, Data, NomeProdutoInterno) VALUES (?, ?, ?, ?) ", [row['QtdeEntrega'] , row['idProdutoFarmacia'], row['Data'], row['NomeProdutoInterno']])
    db.commit()

  return One

def writeOut(One):
  for i in One:
    Rows = One[i]
    for row in Rows:
      node = Rows[row]
      print(node)
      """
      db.execute("INSERT INTO report2013 (Data, idProdutoFarmacia, NomeProdutoInterno, \
          valBaixo, valMedio, valMAlto, QtdeEntrada, TipoMedicamento, TipoGenerico, \
          TipoMaterial, idMedicamentoPrincipioAtivo, QtdEntregue ) \
          VALUES \
              (?,?,?,?,?,?,?,?,?,?,?,?)", \
              [node['Data'], node['idProdutoFarmacia'], node['NomeProdutoInterno'], \
              node['Baixo'], node['Media'], node['MAlto'], node['QtdeEntrada'], \
              node['TipoMedicamento'], node['TipoGenerico'], \
              node['TipoMaterial'], node['idMedicamentoPrincipioAtivo'], node['QtdeEntrega'] \
              ])
      db.commit()
    """
  return One

if __name__ == '__main__':
  One = readOne(One)
  One = readTwo(One)
  print("ending...")
#  writeOut(One)

